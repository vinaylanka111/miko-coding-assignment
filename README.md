# Miko Coding Assignment

## Introduction

Implemented the RRT Algorithm from scratch with suitable data structures.
Used the RRT Algorithm as it's a decent waypoint based navigation method and also follows the description given in the problem statement.

Every node in the RRT Tree has data about the manhattan and euclidean distance to the parent and a cumilative euclidean and manhattan distance to the robot base position.

Used OpenCV to visualise the output tree and relationship with the obstacles.

## Steps to run

- Install OpenCV and make sure it's added to the right path
- Clone this repo and run the output binary with ./bin/rrt_output

In case of any changes (number of iterations for example), rebuild the script with - 
- cmake --build build -- -j3

In case of any changes in build file - 
- cmake -H. -Bbuild

## Assumptions and Constraints

The obstacles are taken from the obstacles.txt file. I've currently added overlapping obstacles to test out the versatility of the algorithm. 

The [-1,-1] position of the robot is given by the top left corner of the OpenCV output, as OpenCV consideres the origin to be at the top left.

### Problem statement pdf attached.