#include <iostream>
#include <algorithm>
#include <utility>
#include <math.h>
#include <random>
#include <vector>
#include <fstream>
#include <bits/stdc++.h>
#include <opencv4/opencv2/opencv.hpp>
#include <string>
#include <filesystem>

using namespace std;

#define RESOLUTION 0.001 // 200x200 grid between (-1,-1) and (1,1)
#define ITERATIONS 1000 // Define number of RRT Iterations

typedef pair<float,float> point;

struct obstacle{
    float x;
    float y;
    float radius;
    obstacle(float a,float b,float r){x=a;y=b;radius=r;}
};
vector <obstacle> obstacle_data;

//Read obstacle data from txt file
void read_obstacles(){
    filesystem::path cwd = filesystem::current_path() / "src/obstacles.txt";
    cout << cwd.string() << endl;
    ifstream in(cwd.string()); // input
    if(!in) {
        cout << "Cannot open obstacles.txt file.\n";
        return;
    }
    float a,b,c;
    while (true) {
        in >> a >> b >> c;
        obstacle data = {a,b,c};
        obstacle_data.push_back(data);
        if( in.eof() ) break;
    }
    
}

float euclidean_dist (float x1, float y1, float x2, float y2) {
    float x = x1 - x2;
    float y = y1 - y2;
    float dist = pow(x, 2) + pow(y, 2);
    dist = sqrt(dist);
    return dist;
}

float manhattan_distance (float x1, float y1, float x2, float y2) {
  float dist;
  int x_dif, y_dif;
  x_dif = x2 - x1;
  y_dif = y2 - y1;
  if(x_dif < 0)
    x_dif = -x_dif;
  if(y_dif < 0)
    y_dif = -y_dif;
  dist = x_dif + y_dif;
  return dist;
}

class rrt_node {
    public:
        point node_coord;
        rrt_node* parent_node = nullptr;
        float prev_node_euclidean,prev_node_manhattan;
        float cumilative_dist_euclidean, cumilative_dist_manhattan;
        vector <rrt_node*> children;
        rrt_node(point coord);
        point get_coord ();
        void set_parent (rrt_node* parent_node);
        void set_child (rrt_node* child_node);
};

vector<rrt_node*> list_of_nodes;

 // Utility function to create a new tree node
rrt_node *newNode(point coord) {
    rrt_node *temp = new rrt_node(coord);
    return temp;
}

rrt_node::rrt_node (point coord){
    node_coord.first = coord.first;
    node_coord.second = coord.second;
}

point rrt_node::get_coord () {
    return node_coord;
}

void rrt_node::set_parent (rrt_node* parent_node) {
    parent_node = parent_node;
}

void rrt_node::set_child (rrt_node* child_node) {
    children.push_back(child_node);
}

bool check_obstacles(point coord) {
    bool status = false;
    for (obstacle obs : obstacle_data) {
        if ((coord.first - obs.x) * (coord.first - obs.x) + (coord.second - obs.y) * (coord.second - obs.y) < (obs.radius * obs.radius))
            return true;
    }
    return false;
}

bool lineSegmentIntersectsCircle(double x1, double y1, double x2, double y2, double x3, double y3, double r) {
//   double dist = std::abs(((y2 - y1) * x3 - (x2 - x1) * y3 + x2 * y1 - y2 * x1) / std::sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1)));
    double m = (y2 - y1) / (x2 - x1);
    double b = y1 - m*x1;
    double dist = std::abs(y3 - m*x3 - b) / std::sqrt(1 + m*m);
  return dist > r;
}

void compute_distances(rrt_node * shortest_node, rrt_node * new_node) {
    new_node->prev_node_euclidean = euclidean_dist(new_node->get_coord().first, new_node->get_coord().second,
                                                    shortest_node->get_coord().first, shortest_node->get_coord().second);
    new_node->prev_node_manhattan = manhattan_distance(new_node->get_coord().first, new_node->get_coord().second,
                                                    shortest_node->get_coord().first, shortest_node->get_coord().second); 
    new_node->cumilative_dist_euclidean = euclidean_dist(new_node->get_coord().first, new_node->get_coord().second,
                                                    list_of_nodes[0]->get_coord().first, list_of_nodes[0]->get_coord().second); 
    new_node->cumilative_dist_manhattan = manhattan_distance(new_node->get_coord().first, new_node->get_coord().second,
                                                    list_of_nodes[0]->get_coord().first, list_of_nodes[0]->get_coord().second);                                             
}

void get_nearest(rrt_node * new_node) {
    float shortest_distance = 2.0;
    rrt_node * shortest_node  = nullptr;
    for (rrt_node * existing_node : list_of_nodes) {
        bool path_test = true;
        //Obstacle Check
        for (obstacle obs : obstacle_data) {
            path_test = lineSegmentIntersectsCircle(new_node->get_coord().first, new_node->get_coord().second,
                                                    existing_node->get_coord().first, existing_node->get_coord().second,
                                                    obs.x, obs.y, obs.radius);
            if(path_test==false){break;}
        }
        if(path_test){
            float temp_shortest_distance = euclidean_dist(new_node->get_coord().first, new_node->get_coord().second,
                                                existing_node->get_coord().first, existing_node->get_coord().second);
            if (temp_shortest_distance < shortest_distance) {
                shortest_distance = temp_shortest_distance;
                shortest_node = existing_node;
            }
        }
    }
    if (shortest_node != nullptr) {
        new_node->set_parent(shortest_node);
        shortest_node->set_child(new_node);
        list_of_nodes.push_back(new_node);
        compute_distances(shortest_node, new_node);
    }
}

// Prints the n-ary tree level wise
void LevelOrderTraversal(rrt_node * root) {
    // Standard level order traversal code
    // using queue
    queue<rrt_node *> q;  // Create a queue
    q.push(root); // Enqueue root
    while (!q.empty()) {
        int n = q.size();
        // If this node has children
        while (n > 0) {
            // Dequeue an item from queue and print it
            rrt_node * p = q.front();
            q.pop();
            cout << p->get_coord().first << "," << p->get_coord().second << " ";
            // cout << p->children.size();
            // Enqueue all children of the dequeued item
            for (int i=0; i<p->children.size(); i++)
                q.push(p->children[i]);
            n--;
        }
  
        cout << endl; // Print new line between two levels
    }
}

//Random point for RRT sampling
//Seeding for random numbers
std::random_device rd;
std::mt19937 mt(rd());
std::uniform_real_distribution<float> samp(-1.0, 1.0);
point sample_rrt(){
    point random;
    random.first = (float)((int)(samp(mt)*1000 + .5)) /1000;
    random.second = (float)((int)(samp(mt)*1000 + .5)) /1000;
    return random;
}

void draw_everything(rrt_node * root) {
    using namespace cv;
    Mat whiteMatrix(1000, 1000, CV_8UC3, Scalar(255, 255, 255));//Declaring a white matrix
    for (obstacle obs : obstacle_data) {
        Point center((0.5 + obs.x/2) * 1000, (0.5+ obs.y/2) * 1000);
        int radius = obs.radius/2 * 1000;
        Scalar line_Color(0, 0, 0);//Color of the circle
        int thickness = 2;//thickens of the line
        namedWindow("whiteMatrix");//Declaring a window to show the circle
        circle(whiteMatrix, center,radius, line_Color, thickness);//Using circle()function to draw the line//
    }
    queue<rrt_node *> q;  // Create a queue
    q.push(root); // Enqueue root
    while (!q.empty()) {
        int n = q.size();
        while (n > 0) {
            rrt_node * p = q.front();
            q.pop();
            Point p1((0.5 + p->get_coord().first/2) * 1000,(0.5 + p->get_coord().second/2) * 1000);
            for (int i=0; i<p->children.size(); i++) {
            Point p2((0.5 + p->children[i]->get_coord().first/2) * 1000,(0.5 + p->children[i]->get_coord().second/2) * 1000);
                line(whiteMatrix, p1, p2, Scalar(255, 0, 0), 2, LINE_8);
                q.push(p->children[i]);
            }
            n--;
        }
  
        cout << endl; // Print new line between two levels
    }
    imshow("RRT Algorithm Output", whiteMatrix);//Showing the circle//
    waitKey(0);//Waiting for Keystroke//
    cv::destroyAllWindows(); //destroy the created window
}


int main() {
    //Create base node at -1,-1
    rrt_node * base_node = newNode(make_pair(-1,-1));
    //Read obstacles from txt file
    read_obstacles();
    list_of_nodes.push_back(base_node);
    //Start RRT for the required number of iterations
    for(int i=0;i<ITERATIONS;i++){
        rrt_node * new_node = newNode(sample_rrt());
        bool test = check_obstacles(new_node->get_coord());
        if(test == true){
            i--;
            continue;
        }
        get_nearest(new_node);
    }
    LevelOrderTraversal(base_node);
    draw_everything(base_node);
    return 0;
}